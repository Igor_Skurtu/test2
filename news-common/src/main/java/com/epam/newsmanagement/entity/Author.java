package com.epam.newsmanagement.entity;

import java.sql.Timestamp;

public class Author {
	
	private long authorId;
	private String authorName;
	private Timestamp expired;
	
	
	public long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public Timestamp getExpired() {
		return expired;
	}
	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}
	
	
}
