package com.epam.newsmanagement.entity;

public class Role {
	
	private long roleId;
	private String roleName;
	
	
	public long getRoleId() {
		return roleId;
	}
	public void setRoleId(long newsId) {
		this.roleId = newsId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	

}
