package com.epam.newsmanagement.entity;

import java.sql.Timestamp;

public class Comment {
	
	private long commentId;
	private long newsId;
	private String commentText;
	private Timestamp creationDate;
	
	
	public long getCommentID() {
		return commentId;
	}
	public void setCommentID(long commentID) {
		this.commentId = commentID;
	}
	public long getNewsID() {
		return newsId;
	}
	public void setNewsID(long newsID) {
		this.newsId = newsID;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	

	
	
}
