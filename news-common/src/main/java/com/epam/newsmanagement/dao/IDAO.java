package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.exception.DAOException;

public interface IDAO<T> {
	
	long create(T t) throws DAOException;
	T read(long id) throws DAOException;
	List<T> readAll() throws DAOException;
	void update(T t, long id) throws DAOException;
	void delete(long id) throws DAOException;
	
}
