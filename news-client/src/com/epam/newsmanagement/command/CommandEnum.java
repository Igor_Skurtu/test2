package com.epam.newsmanagement.command;



public enum CommandEnum {

	GET_ALL_NEWS {
        {
            this.command = new GetAllNewsCommand();
        }
    };
	 ActionCommand command;
    
    public ActionCommand getCommand() {
        return command;
    }
}
